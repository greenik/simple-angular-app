import { Routing } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { MusicModule } from './music/music.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { ItemsListComponent } from './playlists/items-list.component';
import { ListItemComponent } from './playlists/list-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { PlaylistsService } from './playlists/playlists.service';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { ValidateColorDirective } from './playlists/validate-color.directive';
import { TestingComponent } from './testing.component';


@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistContainerComponent,
    HighlightDirective,
    ValidateColorDirective,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MusicModule,
    AuthModule,
    HttpClientModule,
    Routing
  ],
  providers: [
    PlaylistsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private auth: AuthService) {
    auth.getToken();
  }
}
