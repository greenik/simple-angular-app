import { NG_VALIDATORS, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

@Directive({
  selector: '[validateColor]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateColorDirective,
      multi: true
    }
  ]
})
export class ValidateColorDirective implements Validator {

  constructor() {
    // console.log(this);
    // this.model.control.setValidators([
    //   (control) => {
    //     let isError = control.value && control.value.indexOf('#') == -1;
    //     return isError ? {'color': true} : null;
    //   }
    // ])
  }

  validate(control) {
    let isError = control.value && control.value.indexOf('#') == -1;
    return isError ? {'color': true} : null;
  }

}
