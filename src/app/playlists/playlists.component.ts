import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { PlaylistsService } from './playlists.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Playlist } from './playlist';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'playlists',
  templateUrl: './playlists.component.html',
  styles: []
})
export class PlaylistsComponent implements OnInit {

  playlists;

  selected: Observable<Playlist>;

  constructor(private playlistsService: PlaylistsService, private router: Router, private route: ActivatedRoute) { }

  select(playlist: Playlist) {
    this.router.navigate(['/playlists', playlist.id]);
  }

  ngOnInit() {
    this.playlists = this.playlistsService.getPlaylists();
    this.selected = this.route.params.pipe(
      map(params => params['playlist_id']),
      switchMap(id => this.playlistsService.getPlaylist(id))
    );
  }

}
