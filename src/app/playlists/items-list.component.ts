import { Playlist } from './playlist';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'items-list',
  templateUrl: './items-list.component.html',
  styles: [`
    .list-group-item {
      border-left: 10px solid black;
    }
  `]
})
export class ItemsListComponent implements OnInit {
  @Input()
  playlists:Playlist[];

  @Input()
  selected:Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  changeColor(evt) {
    console.log(evt);
  }

  select(playlist:Playlist) {
    this.selectedChange.emit(playlist);
  }

  constructor() { }

  ngOnInit() {
  }

}
