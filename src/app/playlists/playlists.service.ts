import { Playlist } from './playlist';
import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PlaylistsService {
  playlists: Playlist[] = [{
    id: 1,
    name: 'Angular hits',
    color: '#ffff00',
    favourite: false
  },
  {
    id: 2,
    name: 'FOO',
    color: '#00ff00',
    favourite: false
  },
  {
    id: 3,
    name: 'BAR',
    color: '#00ffff',
    favourite: true
  }];

  getPlaylists(): Observable<Playlist[]> {
    return of(this.playlists);
  }

  getPlaylist(id): Observable<Playlist> {
    return of(this.playlists.find(playlist => playlist.id == id));
  }

  savePlaylist(playlist) {
    return of(playlist);
  }
  constructor() { }

}
