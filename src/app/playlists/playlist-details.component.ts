import { NgForm } from '@angular/forms';
import { Component, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'playlist-details',
  templateUrl: './playlist-details.component.html',
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist:Playlist;

  @Output()
  saved = new EventEmitter<Playlist>();

  @ViewChild('playlistForm')
  form: NgForm;

  mode = 'show';

  edit(){
    this.mode = 'edit';
    setTimeout(() => {
      this.form.form.patchValue(this.playlist);
    })
  }

  cancel() {
    this.mode = 'show';
  }

  save(form: NgForm) {
    console.log(form);
    let playlist = {
      ...this.playlist,
      ...form.value
    }
    this.saved.emit(playlist);
  }

  constructor() { }

  ngOnInit() {
  }
  
  ngAfterViewInit() {
  }

}
