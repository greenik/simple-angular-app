import { Observable } from 'rxjs/Rx';
import { switchMap } from 'rxjs/operators/switchMap';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { Playlist } from './playlist';
import { map } from 'rxjs/operators';

@Component({
  selector: 'playlist-container',
  templateUrl: './playlist-container.component.html',
  styles: []
})
export class PlaylistContainerComponent implements OnInit {
  selected: Observable<Playlist>;
  constructor(private playlistsService: PlaylistsService, private route: ActivatedRoute) {
    this.selected = route.params.pipe(
      map(params => params['playlist_id']),
      switchMap(id => this.playlistsService.getPlaylist(id))
    );
  }

  ngOnInit() {
  }

}
