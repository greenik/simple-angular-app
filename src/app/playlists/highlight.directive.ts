import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective implements OnInit {
  color;
  hover;
  @Input('highlight')
  set highlight(color){
    this.color = color;
    this.activeColor = this.hover ? this.color : 'inherit';
  }

  @HostBinding('style.color')
  activeColor

  @HostListener('mouseenter')
  onEnter() {
    this.hover = true;
    this.activeColor = this.color;
  }
  
  @HostListener('mouseleave')
  onLeave() {
    this.hover = false;
    this.activeColor = 'inherit';
  }

  constructor(private elem: ElementRef, private renderer: Renderer2) {
    
  }

  ngOnInit() {
  }

}
