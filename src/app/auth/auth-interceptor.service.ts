import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {

    let authorizedHeaders = req.headers.append('Authorization', 'Bearer ' + this.auth.getToken());
    let authorizedRequest = req.clone({
      headers: authorizedHeaders
    });
    return next.handle(authorizedRequest).pipe(
      catchError(err => {
        if(err instanceof HttpErrorResponse && err.statusText == 'Unauthorized') {
          this.auth.authenticate();
        } else {
          throw err;
        }
        return [];
      })
    );
  }
  constructor(private auth:AuthService) { }

}
