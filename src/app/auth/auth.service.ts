import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  token;

  authenticate() {
    let url = 'https://accounts.spotify.com/authorize',
        response_type = 'token',
        client_id = '22ae76bbcd7d4d59866a78d599c38be6',
        redirect_uri = 'http://localhost:3002/';

    let redirect = `${url}?response_type=${response_type}&client_id=${client_id}&redirect_uri=${redirect_uri}`;
    sessionStorage.removeItem('token');
    window.location.replace(redirect);
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem('token'));
    if(!this.token) {
      let match = window.location.hash.match(/access_token=([^&]*)/);
      this.token = match && match[1];
      window.location.hash = '';
      sessionStorage.setItem('token', JSON.stringify(this.token));
    }
    if(!this.token) {
      this.authenticate();
    }
    return this.token;
  }

  constructor() { }

}
