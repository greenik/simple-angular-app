import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <span>Testing</span>
    <p>{{ message }}</p>
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = "Test message";

  constructor() { }

  ngOnInit() {
  }

}
