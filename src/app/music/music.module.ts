import { MusicSearchService } from './music-search.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [MusicSearchComponent, SearchFormComponent, AlbumsListComponent, AlbumItemComponent],
  exports: [
    MusicSearchComponent
  ],
  providers: [
    {
      provide: 'search_url',
      useValue: 'https://api.spotify.com/v1/search'
    },
    MusicSearchService
  ]
})
export class MusicModule { }
