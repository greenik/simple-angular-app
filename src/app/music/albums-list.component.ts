import { MusicSearchService } from './music-search.service';
import { Component, Input, OnInit } from '@angular/core';
import { Album } from './album';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'albums-list',
  templateUrl: './albums-list.component.html',
  styles: [`
    .card {
      min-width: 25%;
      max-width: 25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {
  albums$:Observable<Album[]>;
  constructor(private musicService:MusicSearchService) { 
    this.albums$ = musicService.getAlbums();
  }

  ngOnInit() {
  }

}
