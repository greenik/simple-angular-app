interface Entity {
    id:string;
}

interface Named {
    name:string;
}

export interface Album extends Entity, Named {
    artists?:Artist[];
    images:AlbumImage[]
}

export interface Artist extends Entity, Named {
    href:string;
}

export interface AlbumImage {
    url:string;
    height:number;
    width:number;
}