import { catchError, map, pluck } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Album } from './album';
import { Injectable, Inject, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

interface APIResponse<Name, T> {
  [Name:string]: PaginationObject<T>
}

interface PaginationObject<T> {
  items: T[],
  total: number;
  limit: number;
}

type AlbumsResponse = APIResponse<'albums', Album>;

@Injectable()
export class MusicSearchService {
  // albums$ = new EventEmitter<Album[]>();
  albums$ = new BehaviorSubject<Album[]>([]);

  search(query) {
    this.http.get<AlbumsResponse>(this.search_url, {
      params: {
        type: 'album',
        query: query
      }
    }).pipe(
      // pluck<any, Album[]>('albums', 'items')
      map(response => response.albums.items)
    ).subscribe(albums => {
      this.albums$.next(albums);
    });
  }

  getAlbums(query = 'Muse') {
    return this.albums$.asObservable();
  }
  constructor(@Inject('search_url') private search_url, private http:HttpClient) {
    
  }

}
