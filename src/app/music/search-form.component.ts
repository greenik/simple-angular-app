import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators, AsyncValidatorFn } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styles: [`
    form.ng-invalid.ng-touched,
    form.ng-invalid.ng-dirty {
      border: 2px solid red !important;
    }
  `]
})

export class SearchFormComponent implements OnInit {
  
  queryForm:FormGroup;
  
  @Output()
  queryChange;

  constructor() {

    const censor = (badword):ValidatorFn => {
      return (control:AbstractControl):ValidationErrors => {
        let hasError = control.value.indexOf(badword) !== -1;
        return hasError ? {
          'censor': badword
        } : null;
      };
    }

    const asyncCensor = (badword):AsyncValidatorFn => {
      return (control:AbstractControl) => {
        return Observable.create(observable => {
          setTimeout(() => {
            observable.next(censor(badword)(control));
            observable.complete();
          }, 2000)
        })
      }
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        censor('batman')
      ], [
        asyncCensor('superman')
      ])
    });
    
    window['form'] = this.queryForm;
    
    let queryField = this.queryForm.get('query');

    let valid$ = queryField.statusChanges.pipe(
      filter(status => status == 'VALID')
    );

    let value$ = queryField.valueChanges;

    this.queryChange = valid$.pipe(
      debounceTime(400),
      withLatestFrom(value$, (valid, value) => value),
      filter(query => query.length >= 3),
      distinctUntilChanged()
    )
  }

  ngOnInit() {
  }

}
