import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from './music-search.service';
import { Album } from './album';

@Component({
  selector: 'music-search',
  templateUrl: './music-search.component.html',
  styles: []
})
export class MusicSearchComponent implements OnInit {
  albums: Album[];
  search(query) {
    this.searchService.search(query);
  }

  constructor(private searchService: MusicSearchService) {

  }

  ngOnInit() {
  }

}
