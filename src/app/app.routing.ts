import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { RouterModule, Routes } from '@angular/router';
import { MusicSearchComponent } from './music/music-search.component';

const routes: Routes = [
  {path: '', redirectTo: 'playlists', pathMatch: 'full'},
  {path: 'playlists', children: [
    {path: '', component: PlaylistsComponent},
    {path: ':playlist_id', component: PlaylistsComponent, children: [
      {path: '', component: PlaylistContainerComponent}
    ]}
  ]},
  {path: 'music', component: MusicSearchComponent},
  {path: '**', redirectTo: 'playlists', pathMatch: 'full'}
];

export const Routing = RouterModule.forRoot(routes, {
  enableTracing: false
});
